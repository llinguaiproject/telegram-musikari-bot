# Telegram Musikari Bot

Telegram eko bot baten bidez musika erreproduzitzeko softwarea. VLC erreproduzitzaile askea erabiliaz
eta PyTube baliatuaz, musika jotzeaz gain youtubetik zure abesti gogokoenak jaitsi ditzazkezu.

Horrez gain, edonondik erabil dezakezu Telegram erabiliaz!

Informazio gehiago Blog-ean aurki dezakezu:


## Instalazioa Linux sistemetan

**Garrantzitsua:** VLC musika erreproduzitzailea Linux sistemetan honako agindua erabiliaz
instalatu:

    sudo apt install vlc

**EZ erabili SNAP!** Bestela ez du eta ongi funtzionatuko

VLC instalatu eta erabiltzeko informazio gehiago [Llinguai Blogean](https://llinguai.blogspot.com/2022/01/vlc-musika-erreproduktore-askea.html)

### Python-eko kanpo-softwarea instalatzen

Gogoratu gomendagarria dela [Ingurune Birtual](https://llinguai.blogspot.com/2021/12/oinarriak-3-proiektu-berri-bat-sortzen.html) bat 
sortzea proiektu bakoitzarentzat.

Ingurune birtuala aktibatu ostean, honako agindua erabil dezakegu linux sistemetan:

    pip3 install -r requirements.txt

Bestela, blogeko Ingurune Birtualen sarreran ikus dezakegu PyCharm erabiliaz nola instalatu 
beharrezkoa dugun kanpo softwarea klik gutxi batzuekin.

### Telegram-eko Bot bat sortzen

Gure bot pertsonalizatua izateko Telegram-en Bot bat sortu beharko dugu, horretarko
jarraitu honako [gida](https://llinguai.blogspot.com/2022/04/telegram-eko-bot-bat-sortzen.html) hau eta
lor ezazu zure bot-aren norbanako Tokena

Behin token-a dugunean .env fitxategian jarri beharko dugu

    TOKEN=53453213:AAHIfFISPgNoxZwxxxXxxxxxxxL_o
    BAIMENDUAK=ErabiltzaileZenbakia,ErabiltzaileZenbakia

Ez badakizu zein den zure erabiltzailea, Bot-arekin hitzegin eta honek erregistroan
idatziko du:

    "[SARBIDEA] Identifikazio zenbakia {} duen erabiltzailea hizketan ari zait!"

Horrela zure musika bot-a zuk nahi duzun pertsonek bakarrik erabili ahal izango dute


# Nola erabili
Honako agindu hauek erabil ditzazkezu Musika bot-a maneiatzeko:

    hasi - Musika Hasi

    pausa - Musika pausatu

    berrabiarazi - Musika berrabiarazi

    yt - Youtube-ko esteka batetik kantua jaitsi eta hasieraratu segituan

    deskargatu - Youtube-ko esteka batetik kantua jaitsi eta hurrengo kantu bezala ipini

    gelditu - Musika gelditu

    hurrena - hurrengo kantura joan

    ezarri - Bilatu kantuaren izenburua eta hasieratu segituan

    gehitu - Bilatu kantuaren izenburua eta hurrengo kantua bezala ipini

    igo - Bolumena igo esandako kopuruan "/igo 20"

    jaitsi - Bolumena jaitsi esandako kopuruan "/jaitsi 15"


## Lizentzia - MIT

Copyright 2022 Llinguai

Honen bidez baimena ematen zaio, kargurik gabe, software honen eta lotutako dokumentazio-fitxategien ("softwarea") kopia bat lortzen duen edonori, Softwarea mugarik gabe erabil dezan, mugarik gabe, softwarea erabiltzeko, kopiatzeko, aldatzeko, fusionatzeko, argitaratzeko, banatzeko, azpilizentziatzeko eta/edo saltzeko eskubideak barne, eta softwarearen kopiak ematen zaizkien pertsonei honako baldintza hauek egiteko baimena eman diezaien:

Aurreko copyright-abisua eta baimen-abisu hori softwarearen kopia edo funtsezko zati guztietan sartuko dira.

SOFTWAREA "DAGOEN BEZALA" EMATEN DA, INOLAKO BERMERIK GABE, EXPLIZITUKI EDO INPLIZITUKI, MERKATURATZE-BERMEETARA, HELBURU PARTIKULAR BATERAKO EGOKITASUNERA ETA EZ-BETETZERA MUGATU GABE. EGILE-ESKUBIDEEN EGILE EDO JABEAK EZ DIRA INOLA ERE IZANGO INOLAKO ERREKLAMAZIO, KALTE EDO BESTELAKO ERANTZUKIZUNEN ERANTZULE, EZ KONTRATU-EKINTZA BATEAN, EZ BIDEGABEKERIAN, EZ BESTE EDOZEIN ARRAZOITAN, SOFTWARETIK KANPO EDO SOFTWAREAREN ERABILERATIK EDO BESTELAKO AKZIOETATIK ERATORRITAKOAK.

