import logging
import os
from dotenv import load_dotenv

load_dotenv()

__version__ = "0.1.1"
__bertsioa__ = __version__
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# Izenburuak atxikitu
lan_ingurunea = os.path.dirname(os.path.abspath(__file__))
musika_karpeta = os.path.join(lan_ingurunea, "..", "musika")

# Ez bada musika karpeta existitzen, berri bat sortu
if not os.path.exists(musika_karpeta):
    os.mkdir(musika_karpeta)

logger = logging.getLogger()
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# Lortu Telegram-eko botaren giltza
TOKEN = os.environ.get("TOKEN", None)
if not TOKEN:
    TOKEN = input("Telegameko Bot-aren Giltza: ")

BAIMENDUAK = os.environ.get("BAIMENDUAK", "")
if BAIMENDUAK:
    BAIMENDUAK = [int(identifikatzailea) for identifikatzailea in BAIMENDUAK.split(',')]
else:
    BAIMENDUAK = []

logger.info("[Orokorra] Baimenduen zerrenda: {}".format(BAIMENDUAK))
