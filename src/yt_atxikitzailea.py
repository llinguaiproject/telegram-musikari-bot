import os
import typing

from pytube import YouTube
from src import musika_karpeta, logger


def _deskargatu_youtubetik(url: str) -> str:
    """
    Bideo baten helbidea pasaz gero, hau deskargatuko du gure musika karpetan, youtubeko izenburarekin
    :param url: helbidea
    :return: Ezer ez
    """
    bideoa = YouTube(url).streams.first()
    izenburua = "{}.mp3".format(bideoa.title)
    if not os.path.exists(os.path.join(musika_karpeta, izenburua)):
        bideoa.download(output_path=musika_karpeta, filename=izenburua)
    return izenburua


def deskargatu_youtubetik(url: str) -> typing.Tuple[bool, str]:
    """
    Kantua atxiki youtubetik
    :param url: Helbidea
    :return:
    """
    try:

        return True, _deskargatu_youtubetik(url)
    except:
        logger.error("Akatsen bat gertatu da YouTubetik deskargatzen", exc_info=True)
        return False, "Arazoren bat izan da zure eskaerarekin"