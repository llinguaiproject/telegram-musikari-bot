from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, CallbackContext

from src.kudeatzailea import KantuenKudeatzailea
from src import logger, TOKEN, BAIMENDUAK


kudeatzailea = KantuenKudeatzailea()

updater = Updater(token=TOKEN, use_context=True)


def baimenduak_soilik(funtzioa):
    def egiaztatu_erabiltzailea(update: Update, context: CallbackContext):
        logger.info("[SARBIDEA] Identifikazio zenbakia {} duen erabiltzailea hizketan ari zait!".format(
            update.effective_chat.id))
        if not BAIMENDUAK:
            # Hutsik badago edonork hitzegin dezake bot honekin
            return funtzioa(update, context)

        if update.effective_chat.id in BAIMENDUAK:
            return funtzioa(update, context)
        else:
            logger.warning("[KONTUZ] {} erabiltzailea Bot-ari hitzegiten ari zaio!".format(update.effective_chat.id))
            return

    return egiaztatu_erabiltzailea


def hasiera(update: Update, context: CallbackContext):
    text = "Ongi etorri. Zure identifikazio zenbakia {} da".format(update.effective_chat.id)
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)


@baimenduak_soilik
def musika_hasi(update: Update, context: CallbackContext):
    erantzuna = kudeatzailea.jarraitu_erreprodukzioa()
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def musika_pausatu(update: Update, context: CallbackContext):
    logger.info("[TG] Musika pausatu dugu")
    erantzuna = kudeatzailea.pausatu_erreproduktorea()
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def musika_gelditu(update: Update, context: CallbackContext):
    erantzuna = kudeatzailea.gelditu_erreproduktorea()
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def musika_berrabiarazi(update: Update, context: CallbackContext):
    erantzuna = kudeatzailea.kantua_berrabiarazi()
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def jaitsi_eta_gehitu(update: Update, context: CallbackContext):
    logger.info("[TG] Musika jeisten eta ilaran gehitzen")
    testua = update.effective_message.text
    helbidea = testua.replace("/jaitsi_eta_gehitu ", '').strip(" ")
    logger.info("[TG] Helbidea: {}".format(helbidea))
    erantzuna = kudeatzailea.deskargatu_eta_gehitu_kantua(helbidea)
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def jaitsi_eta_hasieratu(update: Update, context: CallbackContext):
    logger.info("[TG] Jaitsi eta hasieratu kantua ")
    testua = update.effective_message.text
    helbidea = testua.replace("/jaitsi_eta_hasi ", '').strip(" ")
    logger.info("[TG] Helbidea: {}".format(helbidea))
    erantzuna = kudeatzailea.deskargatu_eta_abiarazi(helbidea)
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def hurrengo_kantua(update: Update, context: CallbackContext):
    logger.info("[TG] Hurrengo kantua!")
    erantzuna = kudeatzailea.hurrengo_kantua()
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def aurreko_kantua(update: Update, context: CallbackContext):
    logger.info("[TG] Aurreko kantua!")
    erantzuna = kudeatzailea.aurreko_kantua()
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def bilatu_kantua_eta_hasi(update: Update, context: CallbackContext):
    logger.info("[TG] Bilatu kantua eta hasieratu ")
    testua = update.effective_message.text
    izenburua = testua.replace("/ezarri ", '').strip(" ")
    logger.info("[TG] Izenburua: {}".format(izenburua))
    erantzuna = kudeatzailea.erreproduzitu_hautatua(izenburua)
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def bilatu_kantua_eta_gehitu(update: Update, context: CallbackContext):
    logger.info("[TG] Bilatu kantua eta gehitu zerrendara ")
    testua = update.effective_message.text
    izenburua = testua.replace("/gehitu ", '').strip(" ")
    logger.info("[TG] Izenburua: {}".format(izenburua))
    erantzuna = kudeatzailea.gehitu_zerrendan_hautatua(izenburua)
    context.bot.send_message(chat_id=update.effective_chat.id, text=erantzuna)


@baimenduak_soilik
def bolumena_igo(update: Update, context: CallbackContext):
    logger.info("[TG] Bolumena igotzen ")
    testua = update.effective_message.text
    zenbatekoa = testua.replace("/igo ", '').strip(" ")
    if zenbatekoa:
        zenbatekoa = int(zenbatekoa)
    else:
        zenbatekoa = None
    kudeatzailea.bolumena_igo(zenbatekoa=zenbatekoa)
    context.bot.send_message(chat_id=update.effective_chat.id, text="Bolumena igotzen")


@baimenduak_soilik
def bolumena_jaitsi(update: Update, context: CallbackContext):
    logger.info("[TG] Bolumena jaisten ")
    testua = update.effective_message.text
    zenbatekoa = testua.replace("/jaitsi ", '').strip(" ")
    if zenbatekoa:
        zenbatekoa = int(zenbatekoa)
    else:
        zenbatekoa = None
    kudeatzailea.bolumena_jaitsi(zenbatekoa=zenbatekoa)
    context.bot.send_message(chat_id=update.effective_chat.id, text="Bolumena jaitsi")


def main():
    commands = [[["hasi"], musika_hasi],
                [["pausa"], musika_pausatu],
                [["berrabiarazi"], musika_berrabiarazi],
                [["yt"], jaitsi_eta_hasieratu],
                [["deskargatu"], jaitsi_eta_gehitu],
                [["gelditu"], musika_gelditu],
                [["hurrena"], hurrengo_kantua],
                [["aurrekoa"], aurreko_kantua],
                [["ezarri"], bilatu_kantua_eta_hasi],
                [["gehitu"], bilatu_kantua_eta_gehitu],
                [["igo"], bolumena_igo],
                [["jaitsi"], bolumena_jaitsi]
                ]
    for command, function in commands:
        updater.dispatcher.add_handler(CommandHandler(command, function))

    callbacks = [
        ["start", hasiera],

    ]
    for callback, function in callbacks:
        updater.dispatcher.add_handler(
            CallbackQueryHandler(pattern=callback, callback=function))

    updater.start_polling(timeout=30)
    updater.idle()
