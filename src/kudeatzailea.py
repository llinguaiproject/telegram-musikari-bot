import threading
import time

import typing
import vlc

import src.kantu_arakatzailea as arakatzailea
from src import erreproduzitzailea, logger, musika_karpeta
from src import yt_atxikitzailea as atxiki


"""

Kudeatzaileak kantuen zerrenda bat maneiatzen du.
Honakoa egin dezake:
2- Aurrekoa

5- Stop
6- Pause
7- Resume
8- Deskargatu
9- Deskargatu eta gehitu
10- Deskargatu eta abiarazi
"""
import os

VLC = vlc.MediaPlayer()
MUSIKA_JO = False


class KantuenKudeatzailea:
    # Zenbat kanturen izenburuak gogorarazi ditzazkegun
    memoria = int(os.environ.get("KANTUEN_MEMORIA", 100))

    def __init__(self):
        self.hurrengoen_kantuen_tuplak = arakatzailea.ausazko_zerrenda()
        self.aurrekoen_zerrenda = []
        self.oraingo_kantua = None
        self._prozesua = threading.Thread(target=self._jarraitu)
        self._prozesua.start()

    def _jarraitu(self):
        while True:
            logger.debug("Musika jo? {}".format(MUSIKA_JO))
            if MUSIKA_JO:
                self.jarraitu_erreprodukzioa()
            time.sleep(2)

    def _ausazkoa_gehitu_kantuetara(self):
        ausazko_kantua = arakatzailea.ausazko_kantua()
        if ausazko_kantua:
            self.hurrengoen_kantuen_tuplak.append(ausazko_kantua)

    def _kantua_hasi(self, kantua):
        global VLC
        VLC.stop()
        fitxategia = os.path.join(musika_karpeta, kantua)
        VLC = vlc.MediaPlayer(fitxategia)
        VLC.play()

    def hurrengo_kantua(self) -> str:
        global MUSIKA_JO, VLC
        if self.hurrengoen_kantuen_tuplak:
            hurrengo_kantua = self.hurrengoen_kantuen_tuplak.pop(0)
            self._kantua_hasi(hurrengo_kantua[0])
            self.aurrekoen_zerrendan_gehitu(self.oraingo_kantua)
            self.oraingo_kantua = hurrengo_kantua
            # Ausazko beste kanta bat gehitu hurrengo kantuetara inoiz zerrenda buka ez dadin
            self._ausazkoa_gehitu_kantuetara()
            MUSIKA_JO = True

            return "Hurrengo kantua hasten: {}".format(self.oraingo_kantua[1])
        else:
            return "Ez duzu kanturik zure karpetan"

    def aurrekoen_zerrendan_gehitu(self, kantua):
        self.aurrekoen_zerrenda.insert(0, kantua)
        self.aurrekoen_zerrenda = self.aurrekoen_zerrenda[:100]

    def gehitu_zerrendan_hautatua(self, izenburua: str):
        fitxategia, izenburua = arakatzailea.bilatu_kantua(izenburua)
        if fitxategia is None:
            return "Kantua ezin izan dut aurkitu!"
        self.hurrengoen_kantuen_tuplak.insert(0, (fitxategia, izenburua))
        return "{} kantua gehitu da zerrendan".format(izenburua)

    def gehitu_zerrendan_fitxategia(self, hautatua: str):
        kantuen_zerrenda, kantuen_izenburuak = arakatzailea.lortu_kantuak()
        for fitxategia, izenburua in zip(kantuen_zerrenda, kantuen_izenburuak):
            if fitxategia == hautatua:
                self.hurrengoen_kantuen_tuplak.insert(0, (fitxategia, izenburua))
                return "{} kantua gehitu da zerrendan".format(izenburua)
        return "Kantua ezin izan dut aurkitu!"

    def erreproduzitu_hautatua(self, izenburua: str):
        fitxategia, izenburua = arakatzailea.bilatu_kantua(izenburua)
        if fitxategia is None:
            return "Kantua ezin izan dut aurkitu!"
        self.hurrengoen_kantuen_tuplak.insert(0, (fitxategia, izenburua))
        return self.hurrengo_kantua()

    def pausatu_erreproduktorea(self) -> str:
        erreproduzitzailea.kantua_pausatu(vlc_erreproduktorea=VLC)
        return "Kantua pausatua"

    def gelditu_erreproduktorea(self) -> str:
        global MUSIKA_JO
        erreproduzitzailea.kantua_gelditu(VLC)
        MUSIKA_JO = False
        return "Kantua gelditua"

    def kantua_berrabiarazi(self) -> str:
        global MUSIKA_JO
        erreproduzitzailea.kantua_berrabiarazi(VLC)
        MUSIKA_JO = True
        return "Kantua berrabiarazita"

    def deskargatu_eta_gehitu_kantua(self, url: str):
        global MUSIKA_JO
        deskargatu_da, erantzuna = atxiki.deskargatu_youtubetik(url)
        if not deskargatu_da:
            return erantzuna
        MUSIKA_JO = True
        return self.gehitu_zerrendan_fitxategia(erantzuna)

    def deskargatu_eta_abiarazi(self, url: str) -> str:
        global MUSIKA_JO, VLC
        deskargatu_da, erantzuna = atxiki.deskargatu_youtubetik(url)
        if not deskargatu_da:
            return erantzuna
        self._kantua_hasi(erantzuna)
        MUSIKA_JO = True
        return erantzuna

    def jarraitu_erreprodukzioa(self) -> str:
        logger.debug("[Kudeatzailea] Hurrengo kantua! Oraingo egoera: {}".format(
            erreproduzitzailea.erreproduzitzailearen_egoera(VLC)))
        if erreproduzitzailea.erreproduzitzailearen_egoera(VLC) in ["State.NothingSpecial", "State.Ended"]:
            return self.hurrengo_kantua()
        return ''

    def ezabatu_kantua(self):
        fitxategia, izena = self.oraingo_kantua
        self.hurrengo_kantua()
        os.remove(fitxategia)
        return "'{}' kantua ezabatua!".format(izena)

    def aurreko_kantua(self) -> str:
        global MUSIKA_JO
        if self.aurrekoen_zerrenda:
            aurreko_kantua, izenburua = self.aurrekoen_zerrenda.pop(0)
            self._kantua_hasi(aurreko_kantua)
            self.oraingo_kantua = (aurreko_kantua, izenburua)
            MUSIKA_JO = True
            return "Aurreko kantua hasten: {}".format(self.oraingo_kantua[1])

        else:
            return "Ez dago aurreko kanturik"

    def bolumena_igo(self, zenbatekoa: typing.Optional[int]):
        if zenbatekoa:
            erreproduzitzailea.bolumena_igo(VLC, zenbatekoa=zenbatekoa)
        else:
            erreproduzitzailea.bolumena_igo(VLC)

    def bolumena_jaitsi(self, zenbatekoa: typing.Optional[int]):
        if zenbatekoa:
            erreproduzitzailea.bolumena_jaitsi(VLC, zenbatekoa=zenbatekoa)
        else:
            erreproduzitzailea.bolumena_jaitsi(VLC)
