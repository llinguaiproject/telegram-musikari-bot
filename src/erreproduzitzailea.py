"""
Fitxategi honetan kantuen VLC erreproduktorea maneiatzeko behar diren aginduak aurkitzen dira
"""


def kantua_gelditu(vlc_erreproduktorea):
    if vlc_erreproduktorea:
        vlc_erreproduktorea.stop()


def kantua_pausatu(vlc_erreproduktorea):
    if vlc_erreproduktorea:
        vlc_erreproduktorea.pause()


def kantua_berrabiarazi(vlc_erreproduktorea):
    if vlc_erreproduktorea:
        vlc_erreproduktorea.play()


def erreproduzitzailearen_egoera(vlc_erreproduktorea) -> str:
    return str(vlc_erreproduktorea.get_state())


def bolumena_igo(vlc_erreproduktorea, zenbatekoa: int = 10):
    oraingo_bolumena = vlc_erreproduktorea.audio_get_volume()
    boluma_igoa = min(100, oraingo_bolumena + zenbatekoa)
    vlc_erreproduktorea.audio_set_volume(boluma_igoa)


def bolumena_jaitsi(vlc_erreproduktorea, zenbatekoa: int = 10):
    oraingo_bolumena = vlc_erreproduktorea.audio_get_volume()
    boluma_igoa = max(0, oraingo_bolumena - zenbatekoa)
    vlc_erreproduktorea.audio_set_volume(boluma_igoa)
