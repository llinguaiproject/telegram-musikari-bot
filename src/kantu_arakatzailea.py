import os
import random
import typing

from thefuzz import fuzz
from thefuzz import process

from src import musika_karpeta


def garbitu_izenburua(izenburua: str) -> str:
    izenburua = izenburua.lower()
    for karakterea in ["_", "-", "?", "!", "(", ")", "[", "]"
                       ".mp3", ",", ".wav", ".mp4", ".ogg", "."]:
        izenburua = izenburua.replace(karakterea, " ")
    # Hutsune bikoitzen bat geratzen bada karaktereen garbiketa ostean, hau deuseztatu
    izenburu_garbia = " ".join([hitza for hitza in izenburua.split(" ") if hitza])
    return izenburu_garbia


def lortu_kantuak():
    kantuen_zerrenda = os.listdir(musika_karpeta)
    kantuen_izenburuak = [garbitu_izenburua(kantua) for kantua in kantuen_zerrenda]
    return kantuen_zerrenda, kantuen_izenburuak


def bilatu_kantua(izenburua):
    izenburua_garbituta = garbitu_izenburua(izenburua)
    fitxategien_zerrenda, kantuen_izenburuak = lortu_kantuak()
    gertuen_duen_izenburua, puntuazioa = process.extractOne(izenburua_garbituta, kantuen_izenburuak,
                                                            scorer=fuzz.partial_token_set_ratio)
    if puntuazioa >= int(os.environ.get("PUNTUAZIO_MINIMOA", 50)):
        fitxategiaren_indizea = kantuen_izenburuak.index(gertuen_duen_izenburua)
        return fitxategien_zerrenda[fitxategiaren_indizea], gertuen_duen_izenburua
    return None, None


def ausazko_kantua() -> typing.Tuple[str, str]:
    fitxategien_zerrenda, kantuen_izenburuak = lortu_kantuak()
    indizea = random.choice(list(range(len(fitxategien_zerrenda))))
    return fitxategien_zerrenda[indizea], kantuen_izenburuak[indizea]


def ausazko_zerrenda() -> typing.List[typing.Tuple[str, str]]:
    fitxategien_zerrenda, kantuen_izenburuak = lortu_kantuak()
    fitxategi_kantu_tuplak = [(fitxategia, kantua) for fitxategia, kantua in
                              zip(fitxategien_zerrenda, kantuen_izenburuak)]
    random.shuffle(fitxategi_kantu_tuplak)
    return fitxategi_kantu_tuplak


